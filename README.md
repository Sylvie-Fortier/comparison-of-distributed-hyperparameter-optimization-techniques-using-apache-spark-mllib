# Comparison of distributed hyperparameter optimization techniques using Apache Spark MLlib  

There is only one jupyter notebook for this project.  

The main objective for this project was to compare GridSearch, RandomSearch, and BayesSearch tuning techniques and determine which one is the best for the Databricks dataset below.  

https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/5915990090493625/2446126855165611/6085673883631125/latest.html  

Basic steps performed were:  
- Fetched datasets from Databricks file storage
- Performed data pre-processing, data cleaning, and basic descriptive statistics
- Joined datasets and dealt with missing values
- Performed correlations to determine which features are more closely related to the target variable and should be considered in the modelling process
- Created data pipelines to train, tune, and cross-validate multiple regression models in a distributed environment using Spark MLlib and MLflow to track the experiments
- Tested BayesSearch for distributed hyperparameter optimization using Hyperopt and Spark MLlib
- Compared regression performance metrics when using RandomSearch, GridSearch, and BayesSearch for hyperparameter optimization.
